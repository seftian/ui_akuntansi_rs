<?php section('css') ?>
<?php endsection() ?>

<?php section('js') ?>
<?php endsection() ?>

<?php section('custom_js') ?>
<?php endsection() ?>

<?php section('content') ?>

  <div class="content-wrapper">
    <section class="content">
      <ol class="breadcrumb">
        <li class="active"><a href="<?= base_url('component/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      </ol>
      
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>150</h3>
              <p>Jumlah Permintaan</p>
            </div>
            <div class="icon">
              <i class="fa fa-list"></i>
            </div>
            <a href="#" class="small-box-footer">Lihat info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-green">
            <div class="inner">
              <h3>53<sup style="font-size: 20px">%</sup></h3>
              <p>Jumlah Transfer</p>
            </div>
            <div class="icon">
              <i class="fa fa-money"></i>
            </div>
            <a href="#" class="small-box-footer">Lihat info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-red">
            <div class="inner">
              <h3>44</h3>
              <p>Transfer Belum Konfirmasi</p>
            </div>
            <div class="icon">
              <i class="fa fa-list-alt"></i>
            </div>
            <a href="#" class="small-box-footer">Lihat info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>65</h3>
              <p>Transfer Telah Konfirmasi</p>
            </div>
            <div class="icon">
              <i class="fa fa-check-square-o"></i>
            </div>
            <a href="#" class="small-box-footer">Lihat info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-file-text"></i> Budget Perencanaan Aktif</h3>
            </div>
            <form role="form">
              <div class="box-body box-spacing">
                <table class="table table-striped table-bordered table-condensed">
                  <thead>
                    <tr>
                      <th>Periode</th>
                      <th>Deskripsi</th>
                      <th width="100px"></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>2016-08-13 s.d. 2016-09-12</td>
                      <td>Lorem ipsum dolorem</td>
                      <td class="text-center">
                        <div class="btn-group">
                          <a href="#" type="button" class="btn btn-primary btn-xs">Isi</a>
                          <a href="#" type="button" class="btn btn-danger btn-xs">Tutup</a>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="box-footer box-spacing">
                <div class="text-center">
                  <a href="#" class="small-box-footer">Lihat Semua Data <i class="fa fa-arrow-circle-o-right"></i></a>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-file-text"></i> Daftar Permintaan</h3>
            </div>
            <form role="form">
              <div class="box-body box-spacing">
                <table class="table table-striped table-bordered table-condensed">
                  <thead>
                    <tr>
                      <th>Unit Kerja</th>
                      <th width="100px">Tanggal</th>
                      <th>Item</th>
                      <th class="text-center" width="50px">Qty</th>
                      <th class="text-center" width="70px">Satuan</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Laboratorium</td>
                      <td>2016-08-15</td>
                      <td>Meja Putar</td>
                      <td class="text-center">24</td>
                      <td class="text-center">kg</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="box-footer box-spacing">
                <div class="text-center">
                  <a href="#" class="small-box-footer">Lihat Semua Data <i class="fa fa-arrow-circle-o-right"></i></a>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="col-md-6">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-file-text"></i> Data Transfer</h3>
            </div>
            <form role="form">
              <div class="box-body box-spacing">
                <table class="table table-striped table-bordered table-condensed">
                  <thead>
                    <tr>
                      <th>Unit Kerja</th>
                      <th width="100px">Tanggal</th>
                      <th>Item</th>
                      <th class="text-center" width="50px">Qty</th>
                      <th class="text-center" width="70px">Satuan</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Apotik 1</td>
                      <td>2016-06-15</td>
                      <td>Kursi Putar</td>
                      <td class="text-center">10</td>
                      <td class="text-center">kg</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="box-footer box-spacing">
                <div class="text-center">
                  <a href="#" class="small-box-footer">Lihat Semua Data <i class="fa fa-arrow-circle-o-right"></i></a>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-file-text"></i> Barang Butuh Pemeliharaan</h3>
            </div>
            <form role="form">
              <div class="box-body box-spacing">
                <table class="table table-striped table-bordered table-condensed">
                  <thead>
                    <tr>
                      <th width="80px">No</th>
                      <th>Nama</th>
                      <th>Spesifikasi</th>
                      <th width="80px">Tanggal</th>
                      <th width="80px">Lokasi</th>
                      <th width="120px"></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>2143432</td>
                      <td>Meja Makan Dorong</td>
                      <td>Kereta Dorong</td>
                      <td>2016-08-15</td>
                      <td>R.Apotik</td>
                      <td class="text-center">
                        <div class="btn-group">
                          <a href="#" type="button" class="btn btn-success btn-xs">Pelihara</a>
                          <a href="#" type="button" class="btn btn-danger btn-xs">Abaikan</a>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="box-footer box-spacing">
                <div class="text-center">
                  <a href="#" class="small-box-footer">Lihat Semua Data <i class="fa fa-arrow-circle-o-right"></i></a>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="col-md-6">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-file-text"></i> Barang Habis</h3>
            </div>
            <form role="form">
              <div class="box-body box-spacing">
                <table class="table table-striped table-bordered table-condensed">
                  <thead>
                    <tr>
                      <th>Item</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Kursi Putar</td>
                    </tr>
                    <tr>
                      <td>Meja Putar</td>
                    </tr>
                    <tr>
                      <td>Kursi Dorong</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="box-footer box-spacing">
                <div class="text-center">
                  <a href="#" class="small-box-footer">Lihat Semua Data <i class="fa fa-arrow-circle-o-right"></i></a>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>

    </section>
  </div>

<?php endsection() ?>
<?php getview('layouts/layout') ?>