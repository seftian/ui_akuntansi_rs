<?php section('css') ?>
<link rel="stylesheet" href="<?= base_url('') ?>public/plugins/datatables/dataTables.bootstrap.css">
<?php endsection('') ?>

<?php section('js') ?>
<script src="<?= base_url('') ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url('') ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<?php endsection('') ?>

<?php section('custom_js') ?>
<script>
  	$(function () {
    	$("#table").DataTable();
  	});
</script>
<?php endsection('') ?>

<?php section('content') ?>

<div class="content-wrapper">
    <section class="content">

    	<div class="alert alert-success alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	        <h4><i class="icon fa fa-check"></i> Sukses !</h4>
	        Data berhasil disimpan.
	    </div>

    	<div class="row">
	        <div class="col-md-12">
	          	<div class="box box-default">
	            	<div class="box-header with-border">
	              		<h3 class="box-title"><i class="fa fa-sitemap"></i> Maping Kode Layanan & Akun</h3>
	            	</div>
	            	<form role="form">
	              		<div class="box-footer box-spacing">
	              			<table id="table" class="table table-condensed table-hover table-striped table-bordered">
		              			<thead>
		              				<tr>
		              					<th class="text-center" width="30px">No</th>
		              					<th class="text-center" width="100px">Kode Layanan</th>
		              					<th>Nama Layanan</th>
		              					<th class="text-center" width="100px">Kode Akun</th>
		              					<th>Nama Akun</th>
		              					<th class="text-center">Status</th>
		              					<th class="text-center" width="110px"></th>
		              				</tr>
		              			</thead>
		              			<tbody>
		              				<tr>
		              					<td></td>
				                      	<td class="text-center">
				                        	<input type="text" class="form-control input-sm" placeholder="Auto" disabled />
				                      	</td>
				                      	<td>
				                        	<select class="form-control input-sm">
				                          		<option>Poliklinik Umum</option>
				                          		<option>Poliklinik Gigi</option>
				                          		<option>Poliklinik Spesialis</option>
				                          		<option>Imunisasi</option>
				                        	</select>
				                      	</td>
				                      	<td>
				                        	<input type="text" class="form-control input-sm" placeholder="Auto" disabled />
				                      	</td>
				                      	<td>
				                        	<select class="form-control input-sm">
				                          		<option>Pendapatan Pemeriksaan Dokter Umum</option>
				                          		<option>Pendapatan Pemeriksaan Dokter Gigi</option>
				                          		<option>Pendapatan Pemeriksaan Dokter Spesialis</option>
				                        	</select>
				                      	</td>
				                      	<td class="text-center">
				                        	<select class="form-control input-sm">
				                          		<option>Debit</option>
				                          		<option>Kredit</option>
				                        	</select>
				                      	</td>
				                      	<td class="text-center">
				                        	<a href="#" type="button" class="btn btn-primary btn-sm">Tambah ke List</a>
				                      	</td>
				                    </tr>

		              				<tr>
		              					<td class="text-center">1</td>
		              					<td class="text-center">401.02</td>
		              					<td>Poliklinik Umum</td>
		              					<td>403.01.01</td>
		              					<td>Pendapatan Pemeriksaan Dokter Umum</td>
		              					<td class="text-center"><span class="btn btn-default btn-xs">Kredit</span></td>
		              					<td class="text-center">
					                        <div class="btn-group">
					                          	<button button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal">Edit</button>
					                        	<button type="button" class="btn btn-danger btn-xs">Hapus</button>
					                        </div>
					                    </td>
		              				</tr>
		              				<tr>
		              					<td class="text-center">2</td>
		              					<td class="text-center">401.03</td>
		              					<td>Poliklinik Gigi</td>
		              					<td>403.01.03</td>
		              					<td>Pendapatan Pemeriksaan Dokter Gigi</td>
		              					<td class="text-center"><span class="btn btn-default btn-xs">Kredit</span></td>
		              					<td class="text-center">
					                        <div class="btn-group">
					                          	<button button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal">Edit</button>
					                        	<button type="button" class="btn btn-danger btn-xs">Hapus</button>
					                        </div>
					                    </td>
		              				</tr>
		              				<tr>
		              					<td class="text-center">3</td>
		              					<td class="text-center">401.04</td>
		              					<td>Poliklinik Spesialis</td>
		              					<td>403.01.05</td>
		              					<td>Pendapatan Pemrikasaan Dokter Spesialis</td>
		              					<td class="text-center"><span class="btn btn-default btn-xs">Kredit</span></td>
		              					<td class="text-center">
					                        <div class="btn-group">
					                          	<button button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal">Edit</button>
					                        	<button type="button" class="btn btn-danger btn-xs">Hapus</button>
					                        </div>
					                    </td>
		              				</tr>
		              				<tr>
		              					<td class="text-center">4</td>
		              					<td class="text-center">401.05</td>
		              					<td>Imunisasi</td>
		              					<td>403.01.06</td>
		              					<td>Pendapatan Imunisasi</td>
		              					<td class="text-center"><span class="btn btn-primary btn-xs">Debit</span></td>
		              					<td class="text-center">
					                        <div class="btn-group">
					                          	<button button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal">Edit</button>
					                        	<button type="button" class="btn btn-danger btn-xs">Hapus</button>
					                        </div>
					                    </td>
		              				</tr>
		              			</tbody>
	              			</table>
	              		</div>
	            	</form>
	            </div>
	        </div>
    	</div>
    </section>
</div>

<?php endsection('') ?>

<?php getview('layouts/layout') ?>
