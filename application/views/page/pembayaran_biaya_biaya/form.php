<?php section('css') ?>
	<link rel="stylesheet" href="<?= base_url('') ?>public/plugins/iCheck/all.css" rel="stylesheet" type="text/css" />
  	<link rel="stylesheet" href="<?= base_url('') ?>public/plugins/datepicker/datepicker3.css">
  	<link rel="stylesheet" href="<?= base_url('') ?>public/plugins/datatables/dataTables.bootstrap.css">
<?php endsection('') ?>

<?php section('js') ?>
  	<script src="<?= base_url('public/plugins/datepicker/bootstrap-datepicker.js') ?>"></script>
	<script src="<?= base_url('') ?>public/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
	<script src="<?= base_url('') ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url('') ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<?php endsection('') ?>

<?php section('custom_js') ?>
<script>
  	$(document).ready(function() {
  		//DatePicker
        $('#datepicker').datepicker({
          autoclose: true
        });
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
          checkboxClass: 'icheckbox_flat-green',
          radioClass: 'iradio_flat-green'
        });

        $('#table').DataTable();
	} );
</script>
<?php endsection('') ?>

<?php section('content') ?>

<div class="content-wrapper">
    <section class="content">
    	<div class="panel panel-default panel-toolbar">
    		<div class="panel-body">
    			<div class="row">
    				<div class="col-md-12">
    					<a href="<?= base_url('pembayaran_biaya_biaya/index') ?>" class="btn btn-sm btn-default pull-right"><span class="fa fa-arrow-left"></span> Kembali</a>
    					<div class="input-group pull-left">
	                      	<button class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i> Perbaharui</button>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>

    	<div class="row">
	        <div class="col-md-12">
	          	<div class="box box-default">
	            	<div class="box-header with-border">
	              		<h3 class="box-title"><i class="fa fa-file-text-o"></i> Form Pembayaran Biaya-biaya</h3>
	            	</div>
	            	<form role="form">
	            		<div class="box-footer box-spacing">
	            			<div class="form-horizontal">
	            				<div class="row">
	            					<div class="col-sm-6">
	            						<div class="form-group">
	            							<label class="col-sm-4 control-label">Tanggal</label>
	            							<div class="col-sm-8">
	            								<div class="input-group">
	            									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
	            									<input type="text" id="datepicker" class="form-control input-sm" placeholder="Masukkan Tanggal Perolehan">
	            								</div>
	            							</div>
	            						</div>
	            						<div class="form-group">
	            							<label class="col-sm-4 control-label">Kredit</label>
	            							<div class="col-sm-8">
	            								<select class="form-control input-sm">
	            									<option>-- Pilih Kredit --</option>
	            								</select>
	            							</div>
	            						</div>
	            					</div>
	            					<div class="col-sm-6">
	            						<div class="form-group">
	            							<label class="col-sm-4 control-label">Dibayar Secara</label>
	            							<div class="col-sm-8">
	            								<input type="checkbox" class="flat-red"/>
	            								<label>Tunai</label>
	            								<input type="checkbox" class="flat-red"/>
	            								<label>Bank</label>
	            							</div>
	            						</div>
	            						<div class="form-group">
	            							<label class="col-sm-4 control-label">Sebesar</label>
	            							<div class="col-sm-8">
	            								<div class="input-group">
	            									<input type="text" class="form-control input-sm" placeholder="0" disabled>
	            									<span class="input-group-addon" style="width: 56%;">Rp. </span>
	            								</div>
	            							</div>
	            						</div>
	            					</div>
	            				</div>
	            			</div>
	            		</div>
		            	<div class="box-header with-border">
		              		<h3 class="box-title"><i class="fa fa-file-text-o"></i> Daftar Transaksi Pembayaran Biaya-biaya</h3>
		            	</div>
            			<div class="box-footer box-spacing table-responsive">
	            			<table id="table" class="table table-condensed table-hover table-striped table-bordered">
		              			<thead>
		              				<tr class="info">
		              					<th>No. TRK</th>
		              					<th>Jenis Biaya</th>
		              					<th>Rinci Biaya</th>
		              					<th>Layanan</th>
		              					<th class="text-center">Nominal</th>
		              					<th>Keterangan</th>
		              				</tr>
		              			</thead>
		              			<tbody>
									<tr>
				                      	<td class="text-center">
				                        	<input type="text" class="form-control input-sm" />
				                      	</td>
				                      	<td>
				                        	<select class="form-control input-sm">
				                          		<option>Penyusutan</option>
				                          		<option>Pemeliharaan</option>
				                        	</select>
				                      	</td>
				                      	<td class="text-center">
				                        	<input type="text" class="form-control input-sm" />
				                      	</td>
				                      	<td>
				                        	<select class="form-control input-sm">
				                          		<option>Pendapatan Pemeriksaan Dokter Umum</option>
				                          		<option>Pendapatan Pemeriksaan Dokter Gigi</option>
				                          		<option>Pendapatan Pemeriksaan Dokter Spesialis</option>
				                        	</select>
				                      	</td>
				                      	<td>
				                        	<input type="text" class="form-control input-sm text-right" />
				                      	</td>
				                      	<td class="text-center">
				                        	<a href="#" type="button" class="btn btn-primary btn-sm">Isi Pembayaran Biaya-biaya</a>
				                      	</td>
				                    </tr>

		              				<tr>
		              					<td></td>
		              					<td></td>
		              					<td></td>
		              					<td></td>
		              					<td class="text-right"></td>
		              					<td></td>
		              				</tr>
		              			</tbody>
	              			</table>
	              		</div>
	            		</div>
	              	</form>
	            </div>
	        </div>
    	</div>
    </section>
</div>

<?php endsection('') ?>

<?php getview('layouts/layout') ?>
