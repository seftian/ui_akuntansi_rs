<?php section('css') ?>
	<link rel="stylesheet" href="<?= base_url('') ?>public/plugins/iCheck/all.css" rel="stylesheet" type="text/css" />
  	<link rel="stylesheet" href="<?= base_url('') ?>public/plugins/datepicker/datepicker3.css">
  	<link rel="stylesheet" href="<?= base_url('') ?>public/plugins/datatables/dataTables.bootstrap.css">
<?php endsection('') ?>

<?php section('js') ?>
  	<script src="<?= base_url('public/plugins/datepicker/bootstrap-datepicker.js') ?>"></script>
	<script src="<?= base_url('') ?>public/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
	<script src="<?= base_url('') ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url('') ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<?php endsection('') ?>

<?php section('custom_js') ?>
<script>
  	$(document).ready(function() {
  		//DatePicker
        $('#datepicker').datepicker({
          autoclose: true
        });
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
          checkboxClass: 'icheckbox_flat-green',
          radioClass: 'iradio_flat-green'
        });

        $('#table').DataTable();
	} );
</script>
<?php endsection('') ?>

<?php section('content') ?>

<div class="content-wrapper">
    <section class="content">

    	<div class="alert alert-success alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	        <h4><i class="icon fa fa-check"></i> Sukses !</h4>
	        Data berhasil disimpan.
	    </div>

    	<div class="row">
	        <div class="col-md-12">
	        	<form role="form">
	        		<div class="nav-tabs-custom">
	        			<ul class="nav nav-tabs">
	        				<li class="active"><a href="#tab_1" data-toggle="tab">Data Pembayaran Biaya-biaya</a></li>
	        				<li><a href="#tab_2" data-toggle="tab">Dihapus &nbsp; &nbsp; <small class="label pull-right bg-red">0</small></a></li>
	        			</ul>
	        			<div class="tab-content">
	        				<div class="tab-pane active" id="tab_1">
	        					<div class="box-body box-spacing">
			            			<div class="form-group col-md-12">
		            					<div class="input-group pull-right">
					                      	<a href="<?= base_url('pembayaran_biaya_biaya/create') ?>" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah</a>
					                      	<button class="btn btn-sm btn-primary"><i class="fa fa-retweet"></i> Otorisasi</button>
					                      	<button class="btn btn-sm btn-primary"><i class="fa fa-folder-open"></i> Buka Otorisasi</button>
					                      	<button class="btn btn-sm btn-default"><i class="fa fa-print"></i> Cetak Slip</button>
		            					</div>
			            			</div>
			            		</div>
			            		<div class="box-footer box-spacing table-responsive">
			            			<table id="table" class="table table-condensed table-hover table-striped table-bordered">
				              			<thead>
				              				<tr class="info">
				              					<th class="text-center" width="20px"><input type="checkbox" class="flat-red"/></th>
				              					<th>Tanggal</th>
				              					<th>Layanan</th>
				              					<th>Metode Pembayaran</th>
				              					<th>Akun</th>
				              					<th class="text-right">Total Kredit</th>
				              					<th class="text-right">Total Debet</th>
				              					<th class="text-center">Otorisasi</th>
				              					<th class="text-center" width="110px"></th>
				              				</tr>
				              			</thead>
				              			<tbody>
				              				<tr>
				              					<td class="text-center"><input type="checkbox" class="flat-red"/></td>
				              					<td>10/10/2016</td>
				              					<td>Poli Gigi</td>
				              					<td>Tunai</td>
				              					<td>100,01</td>
				              					<td class="text-right">Rp. 100.000</td>
				              					<td class="text-right">Rp. 100.000</td>
				              					<td></td>
				              					<td class="text-center">
				              						<div class="btn-group">
							                          	<a href="<?= base_url('jurnal_umum/create') ?>" type="button" class="btn btn-warning btn-xs">Edit</a>
							                        	<button type="button" class="btn btn-danger btn-xs">Hapus</button>
							                        </div>
				              					</td>
				              				</tr>
				              			</tbody>
			              			</table>
			              		</div>
        					</div><!-- /.tab-pane -->
        					<div class="tab-pane" id="tab_2">
        					</div><!-- /.tab-pane -->
        				</div><!-- /.tab-content -->
        			</div><!-- nav-tabs-custom -->
        		</form>
        	</div>
    	</div>
    </section>
</div>

<?php endsection('') ?>

<?php getview('layouts/layout') ?>
