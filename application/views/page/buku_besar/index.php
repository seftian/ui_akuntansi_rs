<?php section('css') ?>
	<link rel="stylesheet" href="<?= base_url() ?>public/plugins/iCheck/all.css" rel="stylesheet" type="text/css" />
  	<link rel="stylesheet" href="<?= base_url('') ?>public/plugins/datepicker/datepicker3.css">
<?php endsection('') ?>

<?php section('js') ?>
  	<script src="<?= base_url('public/plugins/datepicker/bootstrap-datepicker.js') ?>"></script>
	<script src="<?= base_url('') ?>public/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
<?php endsection('') ?>

<?php section('custom_js') ?>
<script>
  	$(document).ready(function() {
  		//DatePicker
        $('#datepicker').datepicker({
          autoclose: true
        });

        $('#datepicker1').datepicker({
          autoclose: true
        });

        $('#datepicker2').datepicker({
          autoclose: true
        });

        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
          checkboxClass: 'icheckbox_flat-green',
          radioClass: 'iradio_flat-green'
        });
	} );
</script>
<?php endsection('') ?>

<?php section('content') ?>

<div class="content-wrapper">
    <section class="content">

    	<div class="alert alert-success alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	        <h4><i class="icon fa fa-check"></i> Sukses !</h4>
	        Data berhasil disimpan.
	    </div>

    	<div class="row">
	        <div class="col-md-12">
	          	<div class="box box-default">
	            	<div class="box-header with-border">
	              		<h3 class="box-title"><i class="fa fa-book"></i> Buku Besar</h3>
	            	</div>
	            	<form role="form">
	              		<div class="box-footer box-spacing">
	              			<div class="form-group col-md-2">
            					<div class="input-group">
            						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            						<input id="datepicker" name="tgl_kirim" class="form-control input-sm" placeholder="Tanggal" type="text">
            					</div>
	            			</div>
	            			<div class="form-group col-md-2">
            					<div class="input-group">
            						<span class="input-group-addon"><i class="fa fa-bookmark"></i></span>
            						<select class="form-control input-sm">
				                        <option>- Kode Jenis Akun -</option>
				                        <option>-</option>
				                        <option>-</option>
				                        <option>-</option>
				                    </select>
            					</div>
	            			</div>
	            			<div class="form-group col-md-2">
            					<div class="input-group">
            						<span class="input-group-addon"><i class="fa fa-bookmark-o"></i></span>
            						<select class="form-control input-sm">
				                        <option>- Akun -</option>
				                        <option>-</option>
				                        <option>-</option>
				                        <option>-</option>
				                    </select>
            					</div>
	            			</div>
	            			<div class="form-group col-md-3">
            					<div class="input-group">
            						<input id="datepicker1" name="" class="form-control input-sm" placeholder="Periode Awal" type="text">
            						<span class="input-group-addon">s.d.</span>
            						<input id="datepicker2" name="" class="form-control input-sm" placeholder="Periode Akhir" type="text">
            					</div>
	            			</div>
	            			<div class="form-group col-md-3">
            					<div class="input-group">
			                      	<button class="btn btn-sm btn-primary">Tampilkan</button>
			                      	<button class="btn btn-sm btn-default">Reset</button>
			                      	<button class="btn btn-sm btn-default">Print File</button>
            					</div>
	            			</div>
	            		</div>
	            		<div class="box-footer box-spacing table-responsive">
	            			<table class="table table-condensed table-hover table-striped table-bordered">
		              			<thead>
		              				<tr class="info">
		              					<th class="text-center" width="20px"><input type="checkbox" class="flat-red"/></th>
		              					<th class="text-center">Tanggal</th>
		              					<th>No. Transaksi</th>
		              					<th>No. Reff</th>
		              					<th class="text-right">Nominal Debet</th>
		              					<th class="text-right">Nominal Kredit</th>
		              					<th class="text-right">Saldo</th>
		              				</tr>
		              			</thead>
		              			<tbody>
		              				<tr>
		              					<td class="text-center"><input type="checkbox" class="flat-red"/></td>
		              					<td class="text-center">12/02/2016</td>
		              					<td>12032930</td>
		              					<td>93212033</td>
		              					<td class="text-right">Rp.</td>
		              					<td class="text-right">Rp.</td>
		              					<td class="text-right">Rp.</td>
		              				</tr>
		              				<tr>
		              					<td class="text-center"><input type="checkbox" class="flat-red"/></td>
		              					<td class="text-center">13/03/2016</td>
		              					<td>32402144</td>
		              					<td>34023244</td>
		              					<td class="text-right">Rp.</td>
		              					<td class="text-right">Rp.</td>
		              					<td class="text-right">Rp.</td>
		              				</tr>
		              				<tr>
		              					<td class="text-center"><input type="checkbox" class="flat-red"/></td>
		              					<td class="text-center">15/02/2016</td>
		              					<td>53224634</td>
		              					<td>74245456</td>
		              					<td class="text-right">Rp.</td>
		              					<td class="text-right">Rp.</td>
		              					<td class="text-right">Rp.</td>
		              				</tr>
		              			</tbody>
		              			<tfoot>
		              				<tr>
		              					<th colspan="4" class="text-center info">Total</th>
		              					<th class="text-right success">Rp. 0</th>
		              					<th class="text-right success">Rp. 0</th>
		              					<th class="text-right success">Rp. 0</th>
		              				</tr>
		              			</tfoot>
	              			</table>
	              		</div>
	            	</form>
	            </div>
	        </div>
    	</div>
    </section>
</div>

<?php endsection('') ?>

<?php getview('layouts/layout') ?>
