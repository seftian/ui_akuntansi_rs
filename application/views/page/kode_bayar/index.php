<?php section('css') ?>
<link rel="stylesheet" href="<?= base_url('') ?>public/plugins/datatables/dataTables.bootstrap.css">
<?php endsection('') ?>

<?php section('js') ?>
<script src="<?= base_url('') ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url('') ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<?php endsection('') ?>

<?php section('custom_js') ?>
<script>
  	$(document).ready(function() {
	    $('#table').DataTable();
	} );
</script>
<?php endsection('') ?>

<?php section('content') ?>

<div class="content-wrapper">
    <section class="content">

    	<div class="alert alert-success alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	        <h4><i class="icon fa fa-check"></i> Sukses !</h4>
	        Data berhasil disimpan.
	    </div>

    	<div class="row">
	        <div class="col-md-12">
	          	<div class="box box-default">
	            	<div class="box-header with-border">
	              		<h3 class="box-title"><i class="fa fa-qrcode"></i> Kode Bayar</h3>
	            	</div>
	            	<form role="form">
	              		<div class="box-footer box-spacing table-responsive">
	              			<table id="table" class="table table-condensed table-hover table-striped table-bordered">
		              			<thead>
		              				<tr>
		              					<th class="text-center" width="30px">No</th>
		              					<th class="text-center">Kode</th>
		              					<th width="500px">Jenis Transaksi</th>
		              					<th>Jenis Bayar</th>
		              					<th class="text-center">Akun</th>
		              					<th>Nama Akun</th>
		              					<th class="text-center">Status</th>
		              					<th class="text-center" width="110px"></th>
		              				</tr>
		              			</thead>
		              			<tbody>
		              				<tr>
		              					<td></td>
				                      	<td class="text-center">
				                        	<input type="text" class="form-control input-sm" />
				                      	</td>
				                      	<td>
				                        	<select class="form-control input-sm">
				                          		<option>Pendapatan</option>
				                          		<option>Pengeluaran</option>
				                        	</select>
				                      	</td>
				                      	<td>
				                        	<select class="form-control input-sm">
				                          		<option>Tunai</option>
				                          		<option>Credit/Debit</option>
				                          		<option>Piutang</option>
				                        	</select>
				                      	</td>
				                      	<td>
				                        	<input type="text" class="form-control input-sm" placeholder="Auto" disabled />
				                      	</td>
				                      	<td>
				                        	<select class="form-control input-sm">
				                          		<option>Kas Layanan</option>
				                          		<option>BCA Tabungan a.n.</option>
				                          		<option>Tagihan Pasien Berlangganan</option>
				                        	</select>
				                      	</td>
				                      	<td>
				                        	<select class="form-control input-sm">
				                          		<option>Debit</option>
				                          		<option>Kredit</option>
				                        	</select>
				                      	</td>
				                      	<td class="text-center">
				                        	<a href="#" type="button" class="btn btn-primary btn-sm">Tambah ke List</a>
				                      	</td>
				                    </tr>

		              				<tr>
		              					<td class="text-center">1</td>
		              					<td class="text-center">001</td>
		              					<td>Pendapatan</td>
		              					<td>Tunai</td>
		              					<td class="text-center">100.01</td>
		              					<td>Kas Layanan</td>
		              					<td class="text-center"><span class="btn btn-primary btn-xs">Debit</span></td>
		              					<td class="text-center">
					                        <div class="btn-group">
					                          	<button button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal">Edit</button>
					                        	<button type="button" class="btn btn-danger btn-xs">Hapus</button>
					                        </div>
					                    </td>
		              				</tr>
		              				<tr>
		              					<td class="text-center">2</td>
		              					<td class="text-center">001</td>
		              					<td>Pendapatan</td>
		              					<td>Credit/Debit</td>
		              					<td class="text-center">101.03.01</td>
		              					<td>BCA Tabungan a.n</td>
		              					<td class="text-center"><span class="btn btn-primary btn-xs">Debit</span></td>
		              					<td class="text-center">
					                        <div class="btn-group">
					                          	<button button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal">Edit</button>
					                        	<button type="button" class="btn btn-danger btn-xs">Hapus</button>
					                        </div>
					                    </td>
		              				</tr>
		              				<tr>
		              					<td class="text-center">3</td>
		              					<td class="text-center">001</td>
		              					<td>Pendapatan</td>
		              					<td>Piutang</td>
		              					<td class="text-center">103.01.02</td>
		              					<td>Tagihan Pasien Berlangganan</td>
		              					<td class="text-center"><span class="btn btn-primary btn-xs">Debit</span></td>
		              					<td class="text-center">
					                        <div class="btn-group">
					                          	<button button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal">Edit</button>
					                        	<button type="button" class="btn btn-danger btn-xs">Hapus</button>
					                        </div>
					                    </td>
		              				</tr>
		              				<tr>
		              					<td class="text-center">4</td>
		              					<td class="text-center">001</td>
		              					<td>Pendapatan</td>
		              					<td>Piutang</td>
		              					<td class="text-center">103.01.02</td>
		              					<td>Tagihan Pasien</td>
		              					<td class="text-center"><span class="btn btn-default btn-xs">Kredit</span></td>
		              					<td class="text-center">
					                        <div class="btn-group">
					                          	<button button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal">Edit</button>
					                        	<button type="button" class="btn btn-danger btn-xs">Hapus</button>
					                        </div>
					                    </td>
		              				</tr>
		              			</tbody>
	              			</table>
	              		</div>
	            	</form>
	            </div>
	        </div>
    	</div>
    </section>
</div>

<?php endsection('') ?>

<?php getview('layouts/layout') ?>
