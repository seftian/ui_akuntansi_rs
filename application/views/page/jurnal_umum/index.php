<?php section('css') ?>
	<link rel="stylesheet" href="<?= base_url('') ?>public/plugins/iCheck/all.css" rel="stylesheet" type="text/css" />
  	<link rel="stylesheet" href="<?= base_url('') ?>public/plugins/datepicker/datepicker3.css">
  	<link rel="stylesheet" href="<?= base_url('') ?>public/plugins/datatables/dataTables.bootstrap.css">
<?php endsection('') ?>

<?php section('js') ?>
  	<script src="<?= base_url('public/plugins/datepicker/bootstrap-datepicker.js') ?>"></script>
	<script src="<?= base_url('') ?>public/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
	<script src="<?= base_url('') ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url('') ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<?php endsection('') ?>

<?php section('custom_js') ?>
<script>
  	$(document).ready(function() {
  		//DatePicker
        $('#datepicker').datepicker({
          autoclose: true
        });
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
          checkboxClass: 'icheckbox_flat-green',
          radioClass: 'iradio_flat-green'
        });

        $('#table').DataTable();
	} );
</script>
<?php endsection('') ?>

<?php section('content') ?>

<div class="content-wrapper">
    <section class="content">

    	<div class="alert alert-success alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	        <h4><i class="icon fa fa-check"></i> Sukses !</h4>
	        Data berhasil disimpan.
	    </div>

    	<div class="row">
	        <div class="col-md-12">
	          	<div class="box box-default">
	            	<div class="box-header with-border">
	              		<h3 class="box-title"><i class="fa fa-book"></i> Jurnal Umum</h3>
	            	</div>
	            	<form role="form">
	              		<div class="box-footer box-spacing">
	            			<div class="form-group col-md-12">
            					<div class="input-group pull-right">
			                      	<a href="<?= base_url('jurnal_umum/create') ?>" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah</a>
			                      	<button class="btn btn-sm btn-primary"><i class="fa fa-thumb-tack"></i> Posting</button>
			                      	<button class="btn btn-sm btn-primary"><i class="fa fa-folder-open"></i> Buka Lagi</button>
			                      	<button class="btn btn-sm btn-default"><i class="fa fa-print"></i> Cetak</button>
            					</div>
	            			</div>
	            		</div>
	            		<div class="box-footer box-spacing table-responsive">
	            			<table id="table" class="table table-condensed table-hover table-striped table-bordered">
		              			<thead>
		              				<tr class="info">
		              					<th class="text-center" width="20px"><input type="checkbox" class="flat-red"/></th>
		              					<th>Tanggal Dokumen</th>
		              					<th>No. Dokumen</th>
		              					<th>Deskripsi</th>
		              					<th class="text-center">Status</th>
		              					<th class="text-center" width="110px"></th>
		              				</tr>
		              			</thead>
		              			<tbody>
		              				<tr>
		              					<td class="text-center"><input type="checkbox" class="flat-red"/></td>
		              					<td>12/02/2016</td>
		              					<td>JME-20160511-001</td>
		              					<td>xx</td>
		              					<td class="text-center">
		              						<span class="btn btn-xs btn-success">Open</span>
		              					</td>
		              					<td class="text-center">
		              						<div class="btn-group">
					                          	<a href="<?= base_url('jurnal_umum/create') ?>" type="button" class="btn btn-warning btn-xs">Edit</a>
					                        	<button type="button" class="btn btn-danger btn-xs">Hapus</button>
					                        </div>
		              					</td>
		              				</tr>
		              				<tr>
		              					<td class="text-center"><input type="checkbox" class="flat-red"/></td>
		              					<td>22/05/2016</td>
		              					<td>JME-20160522-001</td>
		              					<td>xx</td>
		              					<td class="text-center">
		              						<span class="btn btn-xs btn-success">Open</span>
		              					</td>
		              					<td class="text-center">
		              						<div class="btn-group">
					                          	<a href="<?= base_url('jurnal_umum/create') ?>" type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal">Edit</a>
					                        	<button type="button" class="btn btn-danger btn-xs">Hapus</button>
					                        </div>
		              					</td>
		              				</tr>
		              				<tr>
		              					<td class="text-center"><input type="checkbox" class="flat-red"/></td>
		              					<td>18/07/2016</td>
		              					<td>JME-20160718-001</td>
		              					<td>xx</td>
		              					<td class="text-center">
		              						<span class="btn btn-xs btn-success">Open</span>
		              					</td>
		              					<td class="text-center">
		              						<div class="btn-group">
					                          	<a href="<?= base_url('jurnal_umum/create') ?>" type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal">Edit</a>
					                        	<button type="button" class="btn btn-danger btn-xs">Hapus</button>
					                        </div>
		              					</td>
		              				</tr>
		              				<tr>
		              					<td class="text-center"><input type="checkbox" class="flat-red"/></td>
		              					<td>01/08/2016</td>
		              					<td>JME-20160801-001</td>
		              					<td>xx</td>
		              					<td class="text-center">
		              						<span class="btn btn-xs btn-success">Open</span>
		              					</td>
		              					<td class="text-center">
		              						<div class="btn-group">
					                          	<a href="<?= base_url('jurnal_umum/create') ?>" type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal">Edit</a>
					                        	<button type="button" class="btn btn-danger btn-xs">Hapus</button>
					                        </div>
		              					</td>
		              				</tr>
		              			</tbody>
	              			</table>
	              		</div>
	            	</form>
	            </div>
	        </div>
    	</div>
    </section>
</div>

<?php endsection('') ?>

<?php getview('layouts/layout') ?>
