<?php section('css') ?>
<link rel="stylesheet" href="<?= base_url() ?>public/plugins/iCheck/all.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?= base_url('') ?>public/plugins/datepicker/datepicker3.css">
<link rel="stylesheet" href="<?= base_url('') ?>public/plugins/datatables/dataTables.bootstrap.css">
<?php endsection('') ?>

<?php section('js') ?>
<script src="<?= base_url('public/plugins/datepicker/bootstrap-datepicker.js') ?>"></script>
<script src="<?= base_url('') ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url('') ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?= base_url('') ?>public/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
<?php endsection('') ?>

<?php section('custom_js') ?>
<script>
  	$(document).ready(function() {
  		$('#datepicker').datepicker({
          autoclose: true
        });
        $('#datepicker1').datepicker({
          autoclose: true
        });
        $('#datepicker2').datepicker({
          autoclose: true
        });
        
	    $('#table').DataTable( {
	        "scrollX": true,
	        "searching": false
	    } );

	    //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
          checkboxClass: 'icheckbox_flat-green',
          radioClass: 'iradio_flat-green'
        });
	} );
</script>
<?php endsection('') ?>

<?php section('content') ?>

<div class="content-wrapper">
    <section class="content">

    	<div class="alert alert-success alert-dismissable">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	        <h4><i class="icon fa fa-check"></i> Sukses !</h4>
	        Data berhasil disimpan.
	    </div>

    	<div class="row">
	        <div class="col-md-12">
	          	<div class="box box-default">
	            	<div class="box-header with-border">
	              		<h3 class="box-title"><i class="fa fa-thumb-tack"></i> Posting Pendapatan</h3>
	            	</div>
	            	<form role="form">
	            		<div class="box-footer box-spacing">
	              			<div class="form-group col-md-2">
            					<div class="input-group">
            						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            						<input id="datepicker" name="tgl_kirim" class="form-control input-sm" placeholder="Tanggal" type="text">
            					</div>
	            			</div>
	            			<div class="form-group col-md-2">
            					<div class="input-group">
            						<span class="input-group-addon"><i class="fa fa-wheelchair"></i></span>
            						<select class="form-control input-sm">
				                        <option disabled>- Jenis Layanan -</option>
				                        <option>Klinik Umum</option>
				                    </select>
            					</div>
	            			</div>
	            			<div class="form-group col-md-2">
            					<div class="input-group">
            						<span class="input-group-addon"><i class="fa fa-money"></i></span>
            						<select class="form-control input-sm">
            							<option disabled>- Jenis Pembayaran -</option>
				                        <option>Semua</option>
				                        <option>Tunai</option>
				                        <option>Debet/Kredit Card</option>
				                        <option>Piutang</option>
				                    </select>
            					</div>
	            			</div>
	            			<div class="form-group col-md-4">
            					<div class="input-group">
            						<input id="datepicker1" name="" class="form-control input-sm" placeholder="Periode Awal" type="text">
            						<span class="input-group-addon">s.d.</span>
            						<input id="datepicker2" name="" class="form-control input-sm" placeholder="Periode Akhir" type="text">
            					</div>
	            			</div>
	            			<div class="form-group col-md-2">
            					<div class="input-group pull-right">
			                      	<button class="btn btn-sm btn-primary">Proses</button>
			                      	<button class="btn btn-sm btn-primary">Tampilkan</button>
            					</div>
	            			</div>
	            		</div>
	            		<div class="box-footer box-spacing nav-tabs-custom">
	            			<ul class="nav nav-tabs" style="margin-bottom: 15px;">
	            				<li class="active"><a href="#tab_1" data-toggle="tab">Pendapatan</a></li>
	            				<li><a href="#tab_2" data-toggle="tab">Hasil Posting Jurnal</a></li>
	            				<li class="pull-right">
	            					<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><i class="fa fa-thumb-tack"></i> Posting Jurnal</button>
	            				</li>
	            			</ul>
	            			<div class="tab-content">
	            				<div class="tab-pane active" id="tab_1">
	            					<table id="table" class="table table-condensed table-hover table-striped table-bordered">
				              			<thead>
				              				<tr>
				              					<th class="text-center" width="20px"><input type="checkbox" class="flat-red"/></th>
				              					<!--<th class="text-center">Kode Unit Layanan</th>-->
				              					<th width="200px">Nama Unit Layanan</th>
				              					<th class="text-center" width="110px">Jenis Bayar</th>
				              					<th class="text-center" width="150px">Tanggal Transaksi</th>
				              					<th class="text-center" width="110px">Nomor KWIT</th>
				              					<th class="text-center" width="110px">Nomor Status</th>
				              					<th width="150px">Nama Pasien</th>
				              					<th class="text-right" width="110px">Jumlah Dibayar</th>
				              					<th class="text-right" width="110px">PPN</th>
				              					<th class="text-right" width="110px">Jumlah Netto</th>
				              					<th class="text-center" width="110">CTK KWT</th>
				              					<th class="text-center" width="110">Shif Kas</th>
				              					<!--<th class="text-center">Kode Perusahaan</th>-->
				              					<th width="180px">Kerjasama Perusahaan</th>
				              					<th class="text-center" width="110px">Nomor Kartu</th>
				              					<!--<th class="text-center">Kode Tindakan</th>-->
				              					<th width="250px">Nama Tindakan</th>
				              				</tr>
				              			</thead>
				              			<tbody>
				              				<tr>
				              					<td class="text-center" width="20px"><input type="checkbox" class="flat-red"/></td>
				              					<!--<td class="text-center">401.02</td>-->
				              					<td>Poliklinik Umum</td>
				              					<td class="text-center">Tunai</td>
				              					<td class="text-center">10/19/2016</td>
				              					<td class="text-center">1034618</td>
				              					<td class="text-center">P000043098</td>
				              					<td>YESLIN,NN</td>
				              					<td class="text-right">Rp. 40.000</td>
				              					<td class="text-right">Rp. 0</td>
				              					<td class="text-right">Rp. 40.000</td>
				              					<td class="text-center">1</td>
				              					<td class="text-center">1</td>
				              					<!--<td class="text-center"></td>-->
				              					<td></td>
				              					<td class="text-center"></td>
				              					<!--<td class="text-center">002001</td>-->
				              					<td>Konsultasi Dokter</td>
				              				</tr>
				              				<tr>
				              					<td class="text-center" width="20px"><input type="checkbox" class="flat-red"/></td>
				              					<!--<td class="text-center">401.02</td>-->
				              					<td>Poliklinik Gigi</td>
				              					<td class="text-center">PIUTANG</td>
				              					<td class="text-center">19/10/2016</td>
				              					<td class="text-center">2035620</td>
				              					<td class="text-center">P00005309</td>
				              					<td>MAS,TN</td>
				              					<td class="text-right">Rp. 40.000</td>
				              					<td class="text-right">Rp. 0</td>
				              					<td class="text-right">Rp. 40.000</td>
				              					<td class="text-center">1</td>
				              					<td class="text-center">1</td>
				              					<!--<td class="text-center"></td>-->
				              					<td>PT PLN</td>
				              					<td class="text-center">0102947499</td>
				              					<!--<td class="text-center">002001</td>-->
				              					<td>Tindakan Tambal Gigi</td>
				              				</tr>
				              			</tbody>
			              			</table>
            					</div><!-- /.tab-pane -->
            					<div class="tab-pane" id="tab_2">
            						<div class="table-responsive">
            							<table class="table table-condensed table-hover table-striped table-bordered">
					              			<thead>
					              				<tr>
					              					<!--<th class="text-center">Kode Unit Layanan</th>-->
					              					<th class="text-center" width="150px">Nomor Jurnal</th>
					              					<th class="text-center" width="150px">Tanggal</th>
					              					<th width="110px">Akun</th>
					              					<th width="250px">Column1</th>
					              					<th>Column2</th>
					              					<th>Column3</th>
					              					<th class="text-right" width="200px">Debet</th>
					              					<th class="text-right" width="200px">Kredit</th>
					              					<th class="text-center">KD Layanan</th>
					              					<th width="250px">Nama Layanan</th>
					              					<th>Column4</th>
					              					<th class="text-center">Jenis Bayar</th>
					              					<th class="text-center">KD Perush</th>
					              					<th>Column5</th>
					              				</tr>
					              			</thead>
					              			<tbody>
					              				<tr>
					              					<td>00119102016</td>
					              					<td class="text-center">10/19/2016</td>
					              					<td>100.01</td>
					              					<td>Kas Layanan</td>
					              					<td></td>
					              					<td></td>
					              					<td class="text-right">Rp. 40.000</td>
					              					<td class="text-right"></td>
					              					<td class="text-center">401.02</td>
					              					<td>Poliklinik Umum</td>
					              					<td></td>
					              					<td class="text-center">Tunai</td>
					              					<td class="text-center"></td>
					              					<td></td>
					              				</tr>
					              				<tr>
					              					<td>00119102016</td>
					              					<td class="text-center">10/19/2016</td>
					              					<td>403.01.01</td>
					              					<td>Pendapatan Pemeriksaan Dokter Umum</td>
					              					<td></td>
					              					<td></td>
					              					<td class="text-right"></td>
					              					<td class="text-right">Rp. 40.000</td>
					              					<td class="text-center">401.02</td>
					              					<td>Poliklinik Umum</td>
					              					<td></td>
					              					<td class="text-center">Tunai</td>
					              					<td class="text-center"></td>
					              					<td></td>
					              				</tr>
					              			</tbody>
				              			</table>
            						</div>
            					</div><!-- /.tab-pane -->
            				</div><!-- /.tab-content -->
            			</div><!-- nav-tabs-custom -->
	            	</form>
	            </div>
	        </div>
    	</div>
    </section>
</div>


<?php endsection('') ?>

<?php getview('layouts/layout') ?>
