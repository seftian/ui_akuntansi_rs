<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran_biaya_biaya extends CI_Controller {
	
	public function index(){
		$this->load->view('page/pembayaran_biaya_biaya/index');
	}
	
	public function create(){
		$this->load->view('page/pembayaran_biaya_biaya/form');
	}
}