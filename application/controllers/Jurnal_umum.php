<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jurnal_umum extends CI_Controller {
	
	public function index(){
		$this->load->view('page/jurnal_umum/index');
	}
	
	public function create(){
		$this->load->view('page/jurnal_umum/form');
	}
}