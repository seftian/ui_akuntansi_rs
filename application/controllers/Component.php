<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Component extends CI_Controller {
	
	public function dashboard(){
		$this->load->view('layouts/dashboard');
	}

	public function logout(){
		$this->load->view('layouts/login');
	}
	
}